package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import id.ac.ui.cs.advprog.tutorial2.exercise1.receiver.Light;

public class LightOnCommand implements Command {

    private Light light;
    private boolean lit;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        lit = light.isLit();
        light.on();
    }

    @Override
    public void undo() {
        if (lit) {
            light.on();
        } else {
            light.off();
        }
    }
}
