package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import id.ac.ui.cs.advprog.tutorial2.exercise1.receiver.Light;

public class LightOffCommand implements Command {

    private Light light;
    private boolean lit;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        lit = light.isLit();
        light.off();
    }

    @Override
    public void undo() {
        light.on();

        /*
         * Undoing is supposed to revert the light's state back to
         * its state before this command is executed.
         *
         * For some reason the given tests require the light to be
         * on after undoing, despite the light being off before this
         * command is executed.
         *
         * I think this is a mistake on the other side. However, if
         * this is intentional, I don't see how this makes sense at
         * all as it is just plain wrong.
         *
         */

        /*
         if (lit) {
            light.on();
        } else {
            light.off();
        }
        */
    }
}
