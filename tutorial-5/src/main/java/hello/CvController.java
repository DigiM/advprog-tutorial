package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    @GetMapping("/cv")
    public String getCv(@RequestParam(name = "visitor", required = false)
                                       String visitor, Model model) {
        if (visitor != null) {
            model.addAttribute("title", visitor + ", I hope you interested to hire me");
        } else {
            model.addAttribute("title", "This is my CV");
        }

        return "cv";
    }

}
