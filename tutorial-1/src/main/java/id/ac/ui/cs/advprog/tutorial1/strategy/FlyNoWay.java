package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyNoWay implements FlyBehavior {

    public void fly() {
        System.out.printf("I can't fly\n");
    }
}
