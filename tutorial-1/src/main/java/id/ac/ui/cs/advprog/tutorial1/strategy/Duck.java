package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public abstract void display();

    public void swim() {
        System.out.printf("All ducks float, even decoys!\n");
    }

    public void setFlyBehavior (FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }
    public void setQuackBehavior (QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }
}
