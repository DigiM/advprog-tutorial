package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyRocketPowered implements FlyBehavior {

    public void fly() {
        System.out.printf("I'm flying with a rocket\n");
    }
}
