package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyWithWings implements FlyBehavior {

    public void fly() {
        System.out.printf("I'm flying!!\n");
    }
}
