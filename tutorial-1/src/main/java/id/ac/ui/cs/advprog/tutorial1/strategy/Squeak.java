package id.ac.ui.cs.advprog.tutorial1.strategy;

public class Squeak implements QuackBehavior {

    public void quack() {
        System.out.printf("Squeak\n");
    }
}
