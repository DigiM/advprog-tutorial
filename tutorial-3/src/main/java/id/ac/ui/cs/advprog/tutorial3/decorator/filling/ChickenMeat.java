package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Food {
    Food food;

    public ChickenMeat(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return String.format("%s, adding chicken meat", food.getDescription());
    }

    @Override
    public double cost() {
        return food.cost() + 4.5;
    }
}
