package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        if (salary < 100000.00) {
            throw new IllegalArgumentException(
                "CTO salary must not be lower than 100 thousand"
            );
        }

        this.name = name;
        this.salary = salary;
        this.role = "CTO";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
