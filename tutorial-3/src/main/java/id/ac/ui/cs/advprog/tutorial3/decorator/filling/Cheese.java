package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Food {
    Food food;

    public Cheese(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return String.format("%s, adding cheese", food.getDescription());
    }

    @Override
    public double cost() {
        return food.cost() + 2;
    }
}
