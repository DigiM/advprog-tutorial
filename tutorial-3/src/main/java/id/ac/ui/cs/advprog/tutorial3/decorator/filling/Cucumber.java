package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Food {
    Food food;

    public Cucumber(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return String.format("%s, adding cucumber", food.getDescription());
    }

    @Override
    public double cost() {
        return food.cost() + 0.4;
    }
}
