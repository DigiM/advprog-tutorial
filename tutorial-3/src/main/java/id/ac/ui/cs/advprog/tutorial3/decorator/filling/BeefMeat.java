package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class BeefMeat extends Food {
    Food food;

    public BeefMeat(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        return String.format("%s, adding beef meat", food.getDescription());
    }

    @Override
    public double cost() {
        return food.cost() + 6;
    }
}
