import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t"
                + String.valueOf(each.getAmount()) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(getTotalAmount()) + "\n";
        result += "You earned "
            + String.valueOf(getFrequentRenterPoints())
            + " frequent renter points";

        return result;
    }

    public String htmlStatement() {
        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "<br>";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Show figures for this rental
            result += "&nbsp;&nbsp;&nbsp;&nbsp;" + each.getMovie().getTitle()
                + "&nbsp;&nbsp;&nbsp;&nbsp;" + String.valueOf(each.getAmount()) + "<br>";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(getTotalAmount()) + "<br>";
        result += "You earned "
            + String.valueOf(getFrequentRenterPoints())
            + " frequent renter points";

        return result;
    }

    public int getFrequentRenterPoints() {
        int frequentRenterPoints = 0;

        for (Rental rental : rentals) {
            // Add frequent renter points
            frequentRenterPoints += rental.getFrequentRenterPoints();
        }

        return frequentRenterPoints;
    }

    public double getTotalAmount() {
        double totalAmount = 0;

        for (Rental rental : rentals) {
            totalAmount += rental.getAmount();
        }

        return totalAmount;
    }
}