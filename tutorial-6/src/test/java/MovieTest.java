import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Objects;

import org.junit.Before;
import org.junit.Test;

public class MovieTest {

    private Movie movie1;
    private Movie movie2;

    @Before
    public void setUp() {
        movie1 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Who Killed Captain Alex? 2: I Killed Him", Movie.NEW_RELEASE);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie1.getTitle());
    }

    @Test
    public void setTitle() {
        movie1.setTitle("Bad Black");

        assertEquals("Bad Black", movie1.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie1.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie1.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie1.getPriceCode());
    }

    @Test
    public void equalsThis() {
        assertTrue(movie1.equals(movie1));
    }

    @Test
    public void equalsNull() {
        assertFalse(movie1.equals(null));
    }

    @Test
    public void equalsOther() {
        assertFalse(movie1.equals(movie2));
    }

    @Test
    public void getHashCode() {
        assertEquals(Objects.hash(movie1.getTitle(), movie1.getPriceCode()), movie1.hashCode());
    }
}