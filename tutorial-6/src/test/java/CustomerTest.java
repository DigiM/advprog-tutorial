import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    private Customer customer;

    private Movie movie1;
    private Movie movie2;
    private Movie movie3;

    private Rental rent1;
    private Rental rent2;
    private Rental rent3;

    @Before
    public void setUp() {
        customer = new Customer("Alice");

        movie1 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Who Killed Captain Alex? 2: I Killed Him", Movie.NEW_RELEASE);
        movie3 = new Movie("Who Killed Captain Alex? 3: Princess Panic", Movie.CHILDREN);

        rent1 = new Rental(movie1, 3);
        rent2 = new Rental(movie2, 1);
        rent3 = new Rental(movie3, 5);

    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent1);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        customer.addRental(rent1);
        customer.addRental(rent2);
        customer.addRental(rent3);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 11"));
        assertTrue(result.contains("3 frequent renter points"));
    }

    @Test
    public void htmlStatementWithSingleMovie() {
        customer.addRental(rent1);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void htmlStatementWithMultipleMovies() {
        customer.addRental(rent1);
        customer.addRental(rent2);
        customer.addRental(rent3);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 11"));
        assertTrue(result.contains("3 frequent renter points"));
    }
}