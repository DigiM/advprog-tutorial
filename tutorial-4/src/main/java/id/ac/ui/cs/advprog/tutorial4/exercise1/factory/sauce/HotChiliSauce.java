package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class HotChiliSauce implements Sauce {
    public String toString() {
        return "Hot Chili Sauce that everyone in Depok loves";
    }
}
