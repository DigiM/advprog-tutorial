package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class DepokCheese implements Cheese {

    public String toString() {
        return "Depok-Style Cheese";
    }
}
