package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Beans implements Veggies {

    public String toString() {
        return "Beans (from Depok)";
    }
}
