package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class SaltedClams implements Clams {

    public String toString() {
        return "Salted Clams from Depok";
    }
}
