package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BeansTest {

    @Test
    public void createBeansTest() {
        Beans beans = new Beans();

        assertEquals("Beans (from Depok)", beans.toString());
    }
}
