package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class VeggiePizzaTest extends PizzaTest {

    private VeggiePizza veggiePizza;

    @Before
    @Override
    public void setUp() {
        super.setUp();
        veggiePizza = new VeggiePizza(pizzaIngredientFactoryMock);
    }

    @Test
    public void prepareVeggiePizzaTest() {
        veggiePizza.prepare();
        assertEquals(veggiePizza.dough.toString(), doughMock.toString());
        assertEquals(veggiePizza.sauce.toString(), sauceMock.toString());
        assertEquals(veggiePizza.cheese.toString(), cheeseMock.toString());

        assertEquals(veggiePizza.veggies.length, veggiesMockArray.length);
        for (int i = 0; i < veggiePizza.veggies.length; i++) {
            assertEquals(veggiePizza.veggies[i].toString(), veggiesMockArray[i].toString());
        }

        assertNull(veggiePizza.clam);
    }
}
