package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BlackOlivesTest {

    @Test
    public void createBlackOlivesTest() {
        BlackOlives blackOlives = new BlackOlives();

        assertEquals("Black Olives", blackOlives.toString());
    }
}
