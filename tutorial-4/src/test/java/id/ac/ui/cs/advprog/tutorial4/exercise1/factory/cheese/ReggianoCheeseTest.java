package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ReggianoCheeseTest {

    @Test
    public void createReggianoCheeseTest() {
        ReggianoCheese reggianoCheese = new ReggianoCheese();

        assertEquals("Reggiano Cheese", reggianoCheese.toString());
    }
}
