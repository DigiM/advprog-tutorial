package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ThinCrustDoughTest {

    @Test
    public void createThinCrustDoughTest() {
        ThinCrustDough thinCrustDough = new ThinCrustDough();

        assertEquals("Thin Crust Dough", thinCrustDough.toString());
    }
}
