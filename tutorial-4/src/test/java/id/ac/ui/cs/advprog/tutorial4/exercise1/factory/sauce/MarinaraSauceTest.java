package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MarinaraSauceTest {

    @Test
    public void createMarinaraSauceTest() {
        MarinaraSauce marinaraSauce = new MarinaraSauce();

        assertEquals("Marinara Sauce", marinaraSauce.toString());
    }
}
