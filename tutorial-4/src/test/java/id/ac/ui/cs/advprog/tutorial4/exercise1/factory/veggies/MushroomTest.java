package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MushroomTest {

    @Test
    public void createMushroomTest() {
        Mushroom mushroom = new Mushroom();

        assertEquals("Mushrooms", mushroom.toString());
    }
}
