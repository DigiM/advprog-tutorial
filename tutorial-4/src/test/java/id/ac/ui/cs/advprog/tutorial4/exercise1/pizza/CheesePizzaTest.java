package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CheesePizzaTest extends PizzaTest {

    private CheesePizza cheesePizza;

    @Before
    @Override
    public void setUp() {
        super.setUp();
        cheesePizza = new CheesePizza(pizzaIngredientFactoryMock);
    }

    @Test
    public void prepareCheesePizzaTest() {
        cheesePizza.prepare();
        assertEquals(cheesePizza.dough.toString(), doughMock.toString());
        assertEquals(cheesePizza.sauce.toString(), sauceMock.toString());
        assertEquals(cheesePizza.cheese.toString(), cheeseMock.toString());

        assertNull(cheesePizza.clam);
        assertNull(cheesePizza.veggies);
    }
}
