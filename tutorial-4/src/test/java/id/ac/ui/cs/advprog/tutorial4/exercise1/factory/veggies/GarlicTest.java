package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class GarlicTest {

    @Test
    public void createGarlicTest() {
        Garlic garlic = new Garlic();

        assertEquals("Garlic", garlic.toString());
    }
}
