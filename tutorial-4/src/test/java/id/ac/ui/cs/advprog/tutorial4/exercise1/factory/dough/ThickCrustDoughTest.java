package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ThickCrustDoughTest {

    @Test
    public void createThickCrustDoughTest() {
        ThickCrustDough thickCrustDough = new ThickCrustDough();

        assertEquals("ThickCrust style extra thick crust dough", thickCrustDough.toString());
    }
}
