package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EggplantTest {

    @Test
    public void createEggplantTest() {
        Eggplant eggplant = new Eggplant();

        assertEquals("Eggplant", eggplant.toString());
    }
}
