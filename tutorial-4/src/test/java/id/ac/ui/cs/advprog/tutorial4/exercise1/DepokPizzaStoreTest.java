package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private DepokPizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void orderCheesePizzaTest() {
        Pizza pizza;

        pizza = depokPizzaStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());
    }

    @Test
    public void orderClamPizzaTest() {
        Pizza pizza;

        pizza = depokPizzaStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza.getName());
    }

    @Test
    public void orderVeggiePizzaTest() {
        Pizza pizza;

        pizza = depokPizzaStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza.getName());
    }

    @Test
    public void orderNonexistentPizzaTest() {
        Pizza pizza;

        pizza = depokPizzaStore.createPizza("");
        assertNull(pizza);
    }
}
