package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ParmesanCheeseTest {

    @Test
    public void createParmesanCheeseTest() {
        ParmesanCheese parmesanCheese = new ParmesanCheese();

        assertEquals("Shredded Parmesan", parmesanCheese.toString());
    }
}
