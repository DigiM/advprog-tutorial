package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class OnionTest {

    @Test
    public void createOnionTest() {
        Onion onion = new Onion();

        assertEquals("Onion", onion.toString());
    }
}
