package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FrozenClamsTest {

    @Test
    public void createFrozenClamsTest() {
        FrozenClams frozenClams = new FrozenClams();

        assertEquals("Frozen Clams from Chesapeake Bay", frozenClams.toString());
    }
}
