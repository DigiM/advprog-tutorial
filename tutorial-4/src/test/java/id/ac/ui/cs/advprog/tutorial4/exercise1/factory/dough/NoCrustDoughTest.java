package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NoCrustDoughTest {

    @Test
    public void createNoCrustDoughTest() {
        NoCrustDough noCrustDough = new NoCrustDough();

        assertEquals("Depok style no crust dough", noCrustDough.toString());
    }
}
