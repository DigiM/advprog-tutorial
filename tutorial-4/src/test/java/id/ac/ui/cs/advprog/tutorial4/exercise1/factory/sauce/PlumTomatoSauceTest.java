package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PlumTomatoSauceTest {

    @Test
    public void createHotChiliSauceTest() {
        PlumTomatoSauce plumTomatoSauce = new PlumTomatoSauce();

        assertEquals("Tomato sauce with plum tomatoes", plumTomatoSauce.toString());
    }
}
