package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RedPepperTest {

    @Test
    public void createRedPepperTest() {
        RedPepper redPepper = new RedPepper();

        assertEquals("Red Pepper", redPepper.toString());
    }
}
