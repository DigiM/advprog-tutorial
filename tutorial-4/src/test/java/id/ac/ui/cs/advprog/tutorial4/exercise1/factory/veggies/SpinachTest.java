package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SpinachTest {

    @Test
    public void createSpinachTest() {
        Spinach spinach = new Spinach();

        assertEquals("Spinach", spinach.toString());
    }
}
