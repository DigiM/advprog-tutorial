package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HotChiliSauceTest {

    @Test
    public void createHotChiliSauceTest() {
        HotChiliSauce hotChiliSauce = new HotChiliSauce();

        assertEquals("Hot Chili Sauce that everyone in Depok loves", hotChiliSauce.toString());
    }
}
