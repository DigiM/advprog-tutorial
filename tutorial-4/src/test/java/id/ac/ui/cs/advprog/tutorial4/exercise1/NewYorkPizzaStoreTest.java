package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    private NewYorkPizzaStore newYorkPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void orderCheesePizzaTest() {
        Pizza pizza;

        pizza = newYorkPizzaStore.createPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza.getName());
    }

    @Test
    public void orderClamPizzaTest() {
        Pizza pizza;

        pizza = newYorkPizzaStore.createPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza.getName());
    }

    @Test
    public void orderVeggiePizzaTest() {
        Pizza pizza;

        pizza = newYorkPizzaStore.createPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza.getName());
    }

    @Test
    public void orderNonexistentPizzaTest() {
        Pizza pizza;

        pizza = newYorkPizzaStore.createPizza("");
        assertNull(pizza);
    }
}
