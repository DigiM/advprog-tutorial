package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MozzarellaCheeseTest {

    @Test
    public void createMozzarellaCheeseTest() {
        MozzarellaCheese mozzarellaCheese = new MozzarellaCheese();

        assertEquals("Shredded Mozzarella", mozzarellaCheese.toString());
    }
}
