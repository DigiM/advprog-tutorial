package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SaltedClamsTest {

    @Test
    public void createSaltedClamsTest() {
        SaltedClams saltedClams = new SaltedClams();

        assertEquals("Salted Clams from Depok", saltedClams.toString());
    }
}
