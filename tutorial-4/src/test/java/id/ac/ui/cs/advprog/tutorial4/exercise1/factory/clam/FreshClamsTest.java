package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FreshClamsTest {

    @Test
    public void createFreshClamsTest() {
        FreshClams freshClams = new FreshClams();

        assertEquals("Fresh Clams from Long Island Sound", freshClams.toString());
    }
}
