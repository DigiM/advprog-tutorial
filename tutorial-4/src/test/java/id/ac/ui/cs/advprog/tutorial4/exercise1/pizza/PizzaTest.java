package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.mockito.Mockito.when;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.mockito.Mock;

public class PizzaTest {
    @Mock
    PizzaIngredientFactory pizzaIngredientFactoryMock;

    @Mock
    Dough doughMock;

    @Mock
    Sauce sauceMock;

    @Mock
    Veggies veggiesMock;
    Veggies[] veggiesMockArray;

    @Mock
    Cheese cheeseMock;

    @Mock
    Clams clamsMock;

    public void setUp() {
        when(doughMock.toString()).thenReturn("Dummy dough");
        when(sauceMock.toString()).thenReturn("Dummy sauce");
        when(veggiesMock.toString()).thenReturn("Dummy veggies");
        when(cheeseMock.toString()).thenReturn("Dummy cheese");
        when(clamsMock.toString()).thenReturn("Dummy mock");

        veggiesMockArray = new Veggies[3];
        veggiesMockArray[0] = veggiesMock;
        veggiesMockArray[1] = veggiesMock;
        veggiesMockArray[2] = veggiesMock;

        when(pizzaIngredientFactoryMock.createCheese()).thenReturn(cheeseMock);
        when(pizzaIngredientFactoryMock.createClam()).thenReturn(clamsMock);
        when(pizzaIngredientFactoryMock.createDough()).thenReturn(doughMock);
        when(pizzaIngredientFactoryMock.createSauce()).thenReturn(sauceMock);
        when(pizzaIngredientFactoryMock.createVeggies()).thenReturn(veggiesMockArray);
    }
}
