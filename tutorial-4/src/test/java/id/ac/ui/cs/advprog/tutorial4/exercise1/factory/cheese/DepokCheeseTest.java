package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DepokCheeseTest {

    @Test
    public void createDepokCheeseTest() {
        DepokCheese depokCheese = new DepokCheese();

        assertEquals("Depok-Style Cheese", depokCheese.toString());
    }
}
