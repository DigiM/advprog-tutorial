import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class ScoreGroupingTest {
    @Test
    public void testGroupByScore() {
        Map<String, Integer> scores = new HashMap<>();

        scores.put("A", 0);
        scores.put("B", 0);
        scores.put("C", 1);

        Map<Integer, List<String>> groupedScores = new HashMap<>();

        groupedScores.put(0, Arrays.asList("A", "B"));
        groupedScores.put(1, Arrays.asList("C"));

        assertEquals(groupedScores, ScoreGrouping.groupByScores(scores));
    }
}